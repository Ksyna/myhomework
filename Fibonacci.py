n = int(input("Enter fib number:"))

def fib(n):
    fib1 = 1
    fib2 = 1
    M ={0:0, 1:1}
    if n in M:
        return M[n]
    M[n] = fib(n-1) + fib(n-2)
    return M[n]
print (fib(n))
